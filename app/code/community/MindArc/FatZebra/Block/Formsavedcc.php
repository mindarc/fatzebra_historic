<?php

class MindArc_FatZebra_Block_Formsavedcc extends Mage_Payment_Block_Form {

    protected $_methodCode = 'fatzebra_saved_cc';

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('mindarc/fatzebra/form_saved_cc.phtml');
    }
    
    public function getMethodCode() {
            return $this->_methodCode;
    }
    
    public function canSave() {
    $cansave = Mage::getStoreConfig('payment/fatzebra/can_save');
    $isLoggedIn = Mage::getSingleton('customer/session')->isLoggedIn();
	if ($cansave && $isLoggedIn) {
            return true;
        }
        return false;
    }
    
    public function hasCustomerToken(){
     $fatzebraCustomer = Mage::getModel('fatzebra/customer');
      return $fatzebraCustomer->getCustomerToken();
    }
    
    public function getMaskedCardNumber() {
      $fatzebraCustomer = Mage::getModel('fatzebra/customer');
      return $fatzebraCustomer->getMaskedCardNumber();
    }
}
